@extends('admin.parent')

@section('title','Dashboard')

@section('style')
    <link rel="stylesheet" href="{{asset('doccure/admin/assets/css/select2.min.css')}}">
@endsection

@section('page-wrapper')
    <div class="row">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">

                    <h4 class="card-title">specialties Form</h4>
                </div>

                <div class="card-body">
                    @if ($errors->any)
                    @foreach ($errors->all() as $error)
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    @endforeach
                @endif

                @if (session()->has('alert-type'))
                <div class="alert {{ session()->get('alert-type') }} alert-dismissible fade show" role="alert">
                    {{ session()->get('message') }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                @endif
                <form  action="{{ route('specialities.update',[$Specialty->id]) }}" method="POST">
                    @csrf
                    @method('PUT')
                        <div class="form-group">
                            <label>Specialty Name</label>
                        <input name="specialtyname" value="{{ $Specialty->specialtyname }}" type="text" class="form-control">
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-3">Specialty img</label>
                            <div class="col-md-12">
                                <input class="form-control" type="file">
                            </div>
                        </div>
                        <div class="status-toggle">
                            <label>Activity status</label>
                            <div class="status-toggle">
                                <input name="status" type="checkbox" id="status_1" class="check"
                                @if ($Specialty->status === "Active")
                                    checked
                                @endif>
                                <label for="status_1" class="checktoggle">checkbox</label>
                            </div>
                        </div>
                        <div class="text-right">
                            <button type="submit" class="btn btn-primary">Save</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>


@endsection

@section('scripts')
    <script src="{{asset('doccure/admin/assets/js/select2.min.js')}}"></script>
@endsection



