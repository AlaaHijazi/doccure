@extends('admin.parent')

@section('title','Doctors')

@section('style')

@endsection

@section('page-title','Doctors')
@section('page-breadcrumb','Doctors')

@section('action')
    <div class="col-sm-5 col">

    <a href="{{ route('doctors.create') }}"  class="btn btn-primary float-right mt-2">Add</a>
    </div>
@endsection

@section('page-wrapper')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>Doctor Name</th>
                                <th>Speciality</th>
                                <th>Member Since</th>
                                <th>Earned</th>
                                <th>Account Status</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($doctors as $doctor)
                            <tr>
                            <td>{{ $doctor->name }}</td>
                                <td>---</td>
                                <td>{{ $doctor->created_at }}</td>
                                <td>---</td>
                                <td>{{ $doctor->status }}</td>
                                <td>
                                    <a class="btn btn-sm bg-success-light"
                                           href="{{ route('doctors.edit',[$doctor->id]) }}">
                                            <i class="fe fe-pencil"></i> Edit
                                        </a>
                                    <a  onclick="confirmDelete(this,'{{ $doctor->id }}')" type="button" class="btn btn-sm bg-danger-light">
                                        <i class="fe fe-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('doccure/js/axios.js') }}"></script>
<script src="{{ asset('doccure/js/sweetalert.js') }}"></script>
<script>
    function confirmDelete (app, id){
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
        deleteCity(app,id);
  }
})
    }

    function deleteCity(app, id){
        axios.delete('/cms/admin/doctors/'+id)
    .then(function (response) {
        // handle success (200 - 300)
        console.log(response);
        app.closest('tr').remove();
        showMessage(response.data);
  })
    .catch(function (error) {
        // handle error (400-500)
        console.log(error.response.data);
  })
    .then(function () {
        // always executed
  });
    };

    function showMessage(data){
        Swal.fire({
            title: data.title,
            text: data.text,
            icon: data.icon,
            showConfirmButton: false,
            timer: 2000,
            }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }
    </script>
@endsection
