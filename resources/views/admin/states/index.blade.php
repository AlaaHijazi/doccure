@extends('admin.parent')

@section('title','States')

@section('style')
asdasd
@endsection

@section('page-title','States')
@section('page-breadcrumb','States')

@section('action')
    <div class="col-sm-5 col">
        <a href="{{ route('states.create') }}"  class="btn btn-primary float-right mt-2">Add State</a>
    </div>
@endsection

@section('page-wrapper')
    <div class="row">
        <div class="col-sm-12">
            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table class="table table-hover table-center mb-0">
                            <thead>
                            <tr>
                                <th>ID</th>
                                <th>Name</th>
                                <th>Status</th>
                                <th>City</th>
                                <th>Created At</th>
                                <th>Settings</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach ($states as $state)
                            <tr>
                                <td>{{ $state->id }}</td>
                                <td>{{ $state->name }}</td>
                                <td>{{ $state->status }}</td>
                                <td>{{ $state->city->name }}</td>
                                <td>{{ $state->created_at }}</td>
                                <td>
                                    <a class="btn btn-sm bg-success-light"
                                           href="{{ route('states.edit',[$state->id]) }}">
                                            <i class="fe fe-pencil"></i> Edit
                                        </a>
                                        <a onclick="confirmDelete(this,'{{ $state->id }}')" type="button" class="btn btn-sm bg-danger-light">
                                            <i class="fe fe-trash"></i> Delete</a>
                                </td>
                            </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="row justify-content-center">
                    {{$states->links()}}
                </div>
            </div>
        </div>
    </div>
@endsection

@section('scripts')
<script src="{{ asset('doccure/js/axios.js') }}"></script>
<script src="{{ asset('doccure/js/sweetalert.js') }}"></script>


<script>
    function confirmDelete (app, id){
    Swal.fire({
  title: 'Are you sure?',
  text: "You won't be able to revert this!",
  icon: 'warning',
  showCancelButton: true,
  confirmButtonColor: '#3085d6',
  cancelButtonColor: '#d33',
  confirmButtonText: 'Yes, delete it!'
}).then((result) => {
  if (result.value) {
        deleteCity(app,id);
  }
})
    }

    function deleteCity(app, id){
        axios.delete('/cms/admin/states/'+id)
    .then(function (response) {
        // handle success (200 - 300)
        console.log(response);
        app.closest('tr').remove();
        showMessage(response.data);
  })
    .catch(function (error) {
        // handle error (400-500)
        console.log(error.response.data);
  })
    .then(function () {
        // always executed
  });
    };

    function showMessage(data){
        Swal.fire({
            title: data.title,
            text: data.text,
            icon: data.icon,
            showConfirmButton: false,
            timer: 2000,
            }).then((result) => {
                    if (result.dismiss === Swal.DismissReason.timer) {
                    console.log('I was closed by the timer')
                }
            })
        }
    </script>
@endsection
