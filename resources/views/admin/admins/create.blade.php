@extends('admin.parent')

@section('title','Dashboard')

@section('style')
    <link rel="stylesheet" href="{{asset('doccure/admin/assets/css/select2.min.css')}}">
@endsection

@section('page-wrapper')
        <div class="row">
            <div class="col-md-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Admins Form</h4>
                    </div>
                    <div class="card-body">

                        @if ($errors->any)
                        @foreach ($errors->all() as $error)
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                {{ $error }}
                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                </button>
                            </div>
                        @endforeach
                    @endif

                    @if (session()->has('alert-type'))
                    <div class="alert {{ session()->get('alert-type') }} alert-dismissible fade show" role="alert">
                        {{ session()->get('message') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    @endif
                        <form action="{{ route('admins.store') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <h4 class="card-title">Admin Information</h4>
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>First Name</label>
                                        <input name="firstname" value="{{ old('firstname') }}" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Last Name</label>
                                        <input name="lastname" value="{{ old('lastname') }}" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Password</label>
                                        <input name="password" value="{{ old('password') }}" type="password" class="form-control">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>City:</label>
                                                <select name="city_id" id="city_id" class="select" onchange="getcitystates()">
                                                    <option>Select City</option>
                                                    @foreach ($cities as $city)
                                                    <option value="{{$city->id}}">{{$city->name}}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>State:</label>
                                                <select name="state_id" id="state_id" class="select">
                                                    <option>Select State</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Gender:</label>
                                                <select name="gender" class="select">
                                                    <option>Select Gender</option>
                                                    <option value="Male">Male</option>
                                                    <option value="Female">Female</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label>Status:</label>
                                                <select name="status" class="select">
                                                    <option>Select Status</option>
                                                    <option value="Active">Active</option>
                                                    <option value="InActive">InActive</option>
                                                    <option value="Blocked">Blocked</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label>Birth Date</label><br>
                                        <input class="form-control" name="date" type="date"><br>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Email</label>
                                        <input name="email" value="{{ old('email') }}"
                                         type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Mobile</label>
                                        <input name="mobile" value="{{ old('mobile') }}" type="text" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label>Image</label>
                                        <input name="admin_image" value="{{ old('image') }}" class="form-control" type="file" accept="image/*">
                                    </div>
                                </div>
                            </div>
                            <div class="text-right">
                                <button type="submit" class="btn btn-primary">Submit</button>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>

@endsection

@section('scripts')
    <script src="{{asset('doccure/admin/assets/js/select2.min.js')}}"></script>
    <script>

        function getcitystates(){
            var selectCity = document.getElementById('city_id').value;
            var selectState = document.getElementById('state_id');
            selectState.length=0;

            @foreach($cities as $city)
            if(selectCity == '{{ $city->id }}'){
                @foreach($city->states as $state)
                    var option = document.createElement('option');
                    option.text = '{{ $state->name }}'
                    option.name = '{{$state->id}}'
                    selectState.add(option);
                @endforeach
            }
            @endforeach
        }
    </script>
@endsection
