<?php

namespace App\Http\Controllers;

use App\City;
use App\Doctor;
use App\Specialty;
use Illuminate\Http\Request;

use function Ramsey\Uuid\v1;

class DoctorController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //

        $doctors = Doctor::with('specialties')->paginate(7);
        return view('admin.doctors.index', ['doctors' => $doctors]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cities = City::with('states')->where('status', '=', 'Active')->get();
        $specialties = Specialty::where('status', '=', 'Active')->get();
        return view('admin.doctors.create', ['specialties' => $specialties, 'cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'specialty_id' => 'required|exists:specialties,id',
            'name' => 'required|string|min:3|max:10',
            'mobile' => 'required|string',
            'email' => 'required|email',
            'city_id' => 'required',
            'gender' => 'required|string',
            'state_id' => 'required',
            'date' => 'required',
            'status' => 'required|string',
            'Pricing' => 'required|string',
            'hour_price' => 'integer',
            'about' => 'string|max:250',
            'doctor_image' => 'required|image',
        ]);
        $doctor = new doctor();
        $doctor->name = $request->get('name');
        $doctor->email = $request->get('email');
        $doctor->specialty_id = $request->get('specialty_id');
        $doctor->about = $request->get('about');
        $doctor->mobile = $request->get('mobile');
        $doctor->pricing = $request->get('Pricing');
        $doctor->hour_price = $request->get('hour_price');
        $doctor->date = $request->get('date');
        $doctor->status = $request->get('status');
        $doctor->city_id = $request->get('city_id');
        $doctor->gender = $request->get('gender');
        $doctor->state_id = $request->get('state_id');
        $doctor->status = $request->has('status') ? 'Active' : 'InActive';

        if ($request->hasFile('doctor_image')) {
            $doctorimage = $request->file('doctor_image');
            $doctoreName = time() . '_' . $request->get('firstname') . '.' . $doctorimage->getClientOriginalExtension();
            $doctorimage->move('images/doctor', $doctoreName);
            $doctor->doctor_image = $doctoreName;
        }

        $isSaved = $doctor->save();
        if ($isSaved) {
            session()->flash('alert-type', 'alert-success');
            session()->flash('message', 'doctor add successfully');
            return redirect()->back();
        } else {
            session()->flash('alert-type', 'alert-danger');
            session()->flash('message', 'failed to create doctor');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
