<?php

namespace App\Http\Controllers;

use App\City;
use CreateCitiesTable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class CityController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $cities = City::withCount('states')->paginate(5);
        return view('admin.cities.index', ['cities' => $cities]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        return view('admin.cities.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validation
        //
        $request->validate([
            'name' => 'required|string|min:3|max:10',
            'status' => 'in:on',
        ], [
            'name.required' => 'Enter city name',
            'name.string' => 'the city name must be string',
            'name.min' => 'name must be at least 3 character length',
            'name.max' => 'the max character length must be 10 character',
        ]);
        $city = new City();
        $city->name = $request->get('name');
        $city->status = $request->has('status') ? 'Active' : 'InActive';
        $isSaved = $city->save();
        if ($isSaved) {
            session()->flash('alert-type', 'alert-success');
            session()->flash('message', 'city add successfully');
            return redirect()->back();
        } else {
            session()->flash('alert-type', 'alert-danger');
            session()->flash('message', 'failed to create city');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $city = City::findOrFail($id);
        return view('admin.cities.edit', ['city' => $city]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request->request->add(['city_id' => $id]);
        $request->validate([
            'city_id' => 'required|integer|exists:cities,id',
            'name' => 'required|string|min:3|max:10|unique:cities,name,' . $id,
            'status' => 'in:on'
        ]);
        $city = City::find($id);
        $city->name = $request->get('name');
        $city->status = $request->has('status') ? 'Active' : 'InActive';
        $isUpdated = $city->save();
        if ($isUpdated) {
            return redirect()->route('cities.index');
        } else {
            return Redirect()->back();
        }
    }



    public function destroy($id)
    {

        $isDeleted = City::destroy($id);
        if ($isDeleted) {
            return response()->json(
                [
                    'title' => 'Success',
                    'text' => 'City Deleted Successfully',
                    'icon' => 'success',
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'title' => 'Failed',
                    'text' => 'failed to delete city',
                    'icon' => 'error',
                ],
                400
            );
        }
    }
    public function showStates($id)
    {
        $states = City::find($id)->states;
        return view('admin.states.index', ['states' => $states]);
    }
}
