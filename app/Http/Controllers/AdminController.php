<?php

namespace App\Http\Controllers;

use App\Admin;
use App\City;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;


class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $admins = admin::all();
        return view('admin.admins.index', ['admins' => $admins]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $cities = City::with('states')->where('status', '=', 'Active')->get();
        return view('admin.admins.create', ['cities' => $cities]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'firstname' => 'required|string|max:10',
            'lastname' => 'required|string|min:3|max:10',
            'email' => 'required|email',
            'password' => 'required|min:6|max:20',
            'city_id' => 'required',
            'gender' => 'required|string',
            'state_id' => 'required',
            'date' => 'required',
            'mobile' => 'required|min:11|numeric',
            'status' => 'required|string',
            'admin_image' => 'required|image',
        ], [
            'firstname.required' => 'Enter the first name',
            'firstname.string' => 'the first must be string',
            'firstname.max' => 'max first name character length must be 10 character',
            'lastname.required' => 'Enter the last name',
            'lastname.string' => 'the last must be string',
            'lastname.max' => 'max last name character length must be 10 character',
        ]);
        // dd($request->all());
        $admin = new admin();
        $admin->firstname = $request->get('firstname');
        $admin->lastname = $request->get('lastname');
        $admin->email = $request->get('email');
        $admin->password = hash::make($request->get('password'));
        $admin->city_id = $request->get('city_id');
        $admin->gender = $request->get('gender');
        $admin->state_id = $request->get('state_id');
        $admin->date = $request->get('date');
        $admin->mobile = $request->get('mobile');
        $admin->status = $request->get('status');

        if ($request->hasFile('admin_image')) {
            $adminImage = $request->file('admin_image');
            $imageName = time() . '_' . $request->get('firstname') . '.' . $adminImage->getClientOriginalExtension();
            $adminImage->move('images/admin', $imageName);
            $admin->admin_image = $imageName;
        }
        $isSaved = $admin->save();
        if ($isSaved) {
            session()->flash('alert-type', 'alert-success');
            session()->flash('message', 'admin add successfully');
            return redirect()->back();
        } else {
            session()->flash('alert-type', 'alert-danger');
            session()->flash('message', 'failed to create admin');
            return redirect()->back();
        }
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $cities = City::with('states')->where('status', '=', 'Active')->get();
        $admin = Admin::findOrFail($id);
        return view('admin.admins.edit', ['admin' => $admin, 'cities' => $cities]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //

        $request->validate([
            'firstname' => 'required|string|max:10',
            'lastname' => 'required|string|max:10',
            // 'password' => 'required|min:6|max:20',
            'city_id' => 'required',
            'state_id' => 'required',
            'gender' => 'required|string',
            'status' => 'required|string',
            'date' => 'required',
            'email' => 'required|email',
            'mobile' => 'required|min:11|numeric',
            // 'admin_image' => 'required|image',
        ]);
        $admin = Admin::find($id);
        $admin->firstname = $request->get('firstname');
        $admin->lastname = $request->get('lastname');
        $admin->city_id = $request->get('city_id');
        $admin->state_id = $request->get('state_id');
        $admin->gender = $request->get('gender');
        $admin->status = $request->get('status');
        $admin->date = $request->get('date');
        $admin->email = $request->get('email');
        $admin->mobile = $request->get('mobile');


        $isUpdated = $admin->save();
        if ($isUpdated) {
            return redirect()->route('admins.index');
        } else {
            return Redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $isDeleted = Admin::destroy($id);
        if ($isDeleted) {
            return response()->json(
                [
                    'title' => 'Success',
                    'text' => 'Admin Deleted Successfully',
                    'icon' => 'success',
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'title' => 'Failed',
                    'text' => 'failed to delete Admin',
                    'icon' => 'error',
                ],
                400
            );
        }
    }
}
