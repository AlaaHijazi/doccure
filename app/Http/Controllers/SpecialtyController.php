<?php

namespace App\Http\Controllers;

use App\Doctor;
use App\Specialty;
use createspecialtiestable;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class SpecialtyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

        //
        // $specialties = Specialty::all();
        $specialties = Specialty::all();
        // $specialties = Specialty::withCount('Doctor')->get();
        return view('admin.specialties.index', ['Specialty' => $specialties]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $specialties = Specialty::all();
        $doctors = Doctor::all();
        return view('admin.specialties.create', ['doctor' => $doctors, 'Specialty' => $specialties]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $request->validate([
            'specialtyname' => 'required|string|min:3|max:20|unique:specialties,specialtyname',
            'status' => 'in:on',
            'specialty_image' => 'required|image',
        ]);
        $Specialty = new Specialty();
        $Specialty->specialtyname = $request->get('specialtyname');
        $Specialty->status = $request->has('status') ? 'Active' : 'InActive';

        if ($request->hasFile('specialty_image')) {
            $specialty_image = $request->file('specialty_image');
            $imageName = time() . '_' . $request->get('specialtyname') . '.' . $specialty_image->getClientOriginalExtension();
            $specialty_image->move('images/specialty', $imageName);
            $Specialty->specialty_image = $imageName;
        }

        $isSaved = $Specialty->save();
        if ($isSaved) {
            session()->flash('alert-type', 'alert-success');
            session()->flash('message', 'Specialty add successfully');
            return redirect()->back();
        } else {
            session()->flash('alert-type', 'alert-danger');
            session()->flash('message', 'failed to create Specialty');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
        $Specialty = Specialty::findOrFail($id);
        return view('admin.specialties.edit', ['Specialty' => $Specialty]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $request->request->add(['Specialty_id' => $id]);
        $request->validate([
            'Specialty_id' => 'required|integer|exists:specialties,id',
            'specialtyname' => 'required|string|min:3|max:20',
            'status' => 'in:on'
        ]);
        $Specialty = Specialty::find($id);
        $Specialty->specialtyname = $request->get('specialtyname');
        $Specialty->status = $request->has('status') ? 'Active' : 'InActive';
        $isUpdated = $Specialty->save();
        if ($isUpdated) {
            return redirect()->route('specialities.index');
        } else {
            return Redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        $isDeleted = specialty::destroy($id);
        if ($isDeleted) {
            return response()->json(
                [
                    'title' => 'Success',
                    'text' => 'City Deleted Successfully',
                    'icon' => 'success',
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'title' => 'Failed',
                    'text' => 'failed to delete city',
                    'icon' => 'error',
                ],
                400
            );
        }
    }
}
