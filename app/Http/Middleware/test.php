<?php

namespace App\Http\Middleware;

use Closure;

class test
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $name = 'Alaaa';
        if ($name != 'Alaa') {
            return abort(403);
        } else {
            return $next($request);
        }
    }
}
