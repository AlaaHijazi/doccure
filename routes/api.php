<?php


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});


// Route::get('test',function(){
//     return response()->json([
//         'status'=> true
//     ]);
// });

// Route::get('customers','Controller@index');
// Route::post('customers','Controller@store');
// // Route::put('customers','Controller@updata'); false edit all customers
// Route::put('customers/{id}','Controller@update');
// // Route::delete('customers','Controller@destroy'); false delete all customers
// Route::delete('customers/{id}','Controller@destroy');

// Route::get('customers/{id}/orders','Controller@index');
// Route::post('customers/{id}/orders','Controller@store');
// // Route::put('customers/{id}/orders','Controller@update'); false edit all orders
// Route::put('customers/{id}/orders/{order_id}','Controller@update');
// // Route::delete('customers/{id}/orders','Controller@destroy'); false delete all orders
// Route::delete('customers/{id}/orders/{order_id}','Controller@destroy');


Route::namespace('API')->group(function () {
    Route::apiResource('cities', 'CityController');
});
