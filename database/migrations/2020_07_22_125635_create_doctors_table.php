<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDoctorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('doctors', function (Blueprint $table) {
            $table->id();
            $table->string('name', 45);
            $table->string('password');
            $table->string('email', 45);
            $table->string('mobile', 45);
            $table->enum('gender', ['Male', 'Female']);
            $table->date('date');
            $table->string('about', 50)->nullable();
            $table->foreignId('specialities_id');
            $table->foreign('specialities_id')->references('id')->on('specialities');
            $table->enum('status', ['Avtive', 'InActive', 'Blocked']);
            $table->enum('pricing', ['Free', 'PerHour']);
            $table->string('hour_price', 3);
            $table->foreignId('states_id');
            $table->foreign('states_id')->references('id')->on('states');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('doctors');
    }
}
