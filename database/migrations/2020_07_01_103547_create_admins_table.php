<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAdminsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admins', function (Blueprint $table) {
            $table->id();
            $table->string('firstname', 45);
            $table->string('lastname', 45);
            $table->string('email', 45)->unique();
            $table->string('password', 100);
            $table->enum('gender', ['Female', 'Male']);
            $table->string('state_id');
            $table->string('admin_image', 100);
            $table->string('city_id');
            $table->date('date');
            $table->string('mobile', 15)->unique();
            $table->enum('status', ['Active', 'InActive', 'Blocked'])->default('Active');
            $table->rememberToken();
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
}
